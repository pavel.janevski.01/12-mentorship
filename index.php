<?php

// Card 1
$w1img = "images/image 1.png";
$w1name = "VM";
$w1desc = "This is a watch. A brand new Watch from $w1name, Waterproof,
stainless steel.";
$w1price = 630.99;
$w1disc = 50;
$w1shipping = 30;
$w1rating = "4.2";
$w1gender = "male";
$w1stock = 55;
$w1sale = $w1price-(($w1disc/100)*$w1price);
$w1totalPrice = $w1sale + $w1shipping;


if ($w1gender == "male"){
  $w1bgColor = "male";
}elseif ($w1gender == "female"){
  $w1bgColor = "female";
}

if ($w1disc>0) {
  $w1pbdt = "discount";
  $w1disctext = "$w1disc% OFF!";
} elseif ($w1disc==0){
  $w1pbdt = "discount-none";
  $w1disctext = "";
}


if ($w1stock > 0){
  $w1stocktext = "In Stock";
  $w1btntxt = "Order";
  $w1btncolor = "btn-success";
  $w1stocktextColor = "text-success";
} elseif ($w1stock == 0){
  $w1stocktext = "Out Of Stock";
  $w1btntxt = "N/A";
  $w1btncolor = "btn-danger";
  $w1stocktextColor = "text-danger";
}

//Card 2
$w2img = "images/image 2.png";
$w2name = "Baume";
$w2desc = "This is a watch. A new Watch from $w2name, Waterproof,
stainless steel.";
$w2price = "422.99";
$w2disc = 0;
$w2shipping = 29.99;
$w2rating = "4.3";
$w2gender = "female";
$w2stock = 1;
$w2sale = $w2price-(($w2disc/100)*$w2price);
$w2totalPrice = $w2sale + $w2shipping;

if ($w2gender == "male"){
  $w2bgColor = "male";
}elseif ($w2gender == "female"){
  $w2bgColor = "female";
}

if ($w2disc>0) {
  $w2pbdt = "discount";
  $w2disctext = "$w1disc% OFF!";
} elseif ($w2disc==0){
  $w2pbdt = "discount-none";
  $w2disctext = "";
}


if ($w2stock > 0){
  $w2stocktext = "In Stock";
  $w2btntxt = "Order";
  $w2btncolor = "btn-success";
  $w2stocktextColor = "text-success";
} elseif ($w2stock == 0){
  $w2stocktext = "Out Of Stock";
  $w2btntxt = "N/A";
  $w2btncolor = "btn-danger";
  $w2stocktextColor = "text-danger";
}

//Card 3
$w3img="images/image 3.png";
$w3name="Titan";
$w3desc="This is a watch. A new Watch from $w3name, Waterproof,
stainless steel.";
$w3price="314.991";
$w3disc=10;
$w3shipping=20;
$w3rating="5";
$w3gender="male";
$w3stock=0;
$w3sale = $w3price-(($w3disc/100)*$w3price);
$w3totalPrice = $w3sale + $w3shipping;

if ($w3gender == "male"){
  $w3bgColor = "male";
}elseif ($w3gender == "female"){
  $w3bgColor = "female";
}

if ($w3disc>0) {
  $w3pbdt = "discount";
  $w3disctext = "$w3disc% OFF!";
} elseif ($w3disc==0){
  $w3pbdt = "discount-none";
  $w3disctext = "";
}


if ($w3stock > 0){
  $w3stocktext = "In Stock";
  $w3btntxt = "Order";
  $w3btncolor = "btn-success";
  $w3stocktextColor = "text-success";
} elseif ($w3stock == 0){
  $w3stocktext = "Out Of Stock";
  $w3btntxt = "N/A";
  $w3btncolor = "btn-danger";
  $w3stocktextColor = "text-danger";
}


// Card 4
$w4img = "images/image 4.png";
$w4name = "Quartz";
$w4desc = "This is a watch. A brand new Watch from $w4name, Waterproof,
stainless steel.";
$w4price = "159.992";
$w4disc = 30;
$w4shipping = 25;
$w4rating = "5";
$w4gender = "female";
$w4stock = 1;
$w4sale = $w4price-(($w4disc/100)*$w4price);
$w4totalPrice = $w4sale + $w4shipping;

if ($w4gender == "male"){
  $w4bgColor = "male";
}elseif ($w4gender == "female"){
  $w4bgColor = "female";
}

if ($w4disc>0) {
  $w4pbdt = "discount";
  $w4disctext = "$w4disc% OFF!";
} elseif ($w4disc==0){
  $w4pbdt = "discount-none";
  $w4disctext = "";
}


if ($w4stock > 0){
  $w4stocktext = "In Stock";
  $w4btntxt = "Order";
  $w4btncolor = "btn-success";
  $w4stocktextColor = "text-success";
} elseif ($w4stock == 0){
  $w4stocktext = "Out Of Stock";
  $w4btntxt = "N/A";
  $w4btncolor = "btn-danger";
  $w4stocktextColor = "text-danger";
}


// Card 5
$w5img = "images/image 5.png";
$w5name = "Lauriel";
$w5desc = "This is a watch. A brand new Watch from $w5name, Waterproof,
stainless steel.";
$w5price = "153.993";
$w5disc = 30;
$w5shipping = 25;
$w5rating = "7.5";
$w5gender = "male";
$w5stock = 0;
$w5sale = $w5price-(($w5disc/100)*$w5price);
$w5totalPrice = $w5sale + $w5shipping;

if ($w5gender == "male"){
  $w5bgColor = "male";
}elseif ($w5gender == "female"){
  $w5bgColor = "female";
}

if ($w5disc>0) {
  $w5pbdt = "discount";
  $w5disctext = "$w5disc% OFF!";
} elseif ($w5disc==0){
  $w5pbdt = "discount-none";
  $w5disctext = "";
}


if ($w5stock > 0){
  $w5stocktext = "In Stock";
  $w5btntxt = "Order";
  $w5btncolor = "btn-success";
  $w5stocktextColor = "text-success";
} elseif ($w5stock == 0){
  $w5stocktext = "Out Of Stock";
  $w5btntxt = "N/A";
  $w5btncolor = "btn-danger";
  $w5stocktextColor = "text-danger";
}


// Card 6
$w6img = "images/image 6.png";
$w6name = "Shengke";
$w6desc = "This is a watch. A brand new Watch from $w6name, Waterproof,
stainless steel.";
$w6price = "99.99";
$w6disc = 0;
$w6shipping = 7.2;
$w6rating = "2.3";
$w6gender = "female";
$w6stock = 1;
$w6sale = $w6price-(($w6disc/100)*$w6price);
$w6totalPrice = $w6sale + $w6shipping;


if ($w6gender == "male"){
  $w6bgColor = "male";
}elseif ($w6gender == "female"){
  $w6bgColor = "female";
}

if ($w6disc>0) {
  $w6pbdt = "discount";
  $w6disctext = "$w6disc% OFF!";
} elseif ($w6disc==0){
  $w6pbdt = "discount-none";
  $w6disctext = "";
}


if ($w6stock > 0){
  $w6stocktext = "In Stock";
  $w6btntxt = "Order";
  $w6btncolor = "btn-success";
  $w6stocktextColor = "text-success";
} elseif ($w6stock == 0){
  $w6stocktext = "Out Of Stock";
  $w6btntxt = "N/A";
  $w6btncolor = "btn-danger";
  $w6stocktextColor = "text-danger";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Bootstrap_JS_Workshop</title>
  <link rel="stylesheet" href="./style.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
      
      <style>
      .male {
        background-color: #f7f7f7;
      }
      .female {
        background-color: #ffe6ee;
      }
      .discount {
        color: gray;
        text-decoration: line-through;
      }
      .discount-none {
        display: none;
      }
      </style>
  </head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-12"><h1>Products</h1></div>
      <!-- Card 1 -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w1bgColor?>">
          <img src="<?php echo $w1img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w1name . " " . $w1disctext?></h5>
            <p class="card-text">
            <?php echo $w1desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w1pbdt?>">US $<?php echo $w1price?></span>US $<?php echo $w1sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w1shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w1rating?></p>
          <p class="card-text ml-3 <?php echo $w1stocktextColor?>"><?php echo $w1stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w1totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w1btncolor?> btn-block"><?php echo $w1btntxt?></a>
          </div>
        </div>
      </div>
      <!-- Card 2 -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w2bgColor?>">
          <img src="<?php echo $w2img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w2name . " " . $w2disctext?></h5>
            <p class="card-text">
            <?php echo $w2desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w2pbdt?>">US $<?php echo $w2price?></span>US $<?php echo $w2sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w2shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w2rating?></p>
          <p class="card-text ml-3 <?php echo $w2stocktextColor?>"><?php echo $w2stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w2totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w2btncolor?> btn-block"><?php echo $w2btntxt?></a>
          </div>
        </div>
      </div>
      <!-- Card 3 -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w3bgColor?>">
          <img src="<?php echo $w3img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w3name . " " . $w3disctext?></h5>
            <p class="card-text">
            <?php echo $w3desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w3pbdt?>">US $<?php echo $w3price?></span>US $<?php echo $w3sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w3shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w3rating?></p>
          <p class="card-text ml-3 <?php echo $w3stocktextColor?>"><?php echo $w3stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w3totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w3btncolor?> btn-block"><?php echo $w3btntxt?></a>
          </div>
        </div>
      </div>
      <!-- Card 4 -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w4bgColor?>">
          <img src="<?php echo $w4img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w4name . " " . $w4disctext?></h5>
            <p class="card-text">
            <?php echo $w4desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w4pbdt?>">US $<?php echo $w4price?></span>US $<?php echo $w4sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w4shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w4rating?></p>
          <p class="card-text ml-3 <?php echo $w4stocktextColor?>"><?php echo $w4stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w4totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w4btncolor?> btn-block"><?php echo $w4btntxt?></a>
          </div>
        </div>
      </div>
      <!-- Card 5 -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w5bgColor?>">
          <img src="<?php echo $w5img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w5name . " " . $w5disctext?></h5>
            <p class="card-text">
            <?php echo $w5desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w5pbdt?>">US $<?php echo $w5price?></span>US $<?php echo $w5sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w5shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w5rating?></p>
          <p class="card-text ml-3 <?php echo $w5stocktextColor?>"><?php echo $w5stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w5totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w5btncolor?> btn-block"><?php echo $w5btntxt?></a>
          </div>
        </div>
      </div>
      <!-- Card 6  -->
      <div class="col-4">
        <div class="card m-3 <?php echo $w6bgColor?>">
          <img src="<?php echo $w6img?>" class="card-img-top img-fluid" alt="Wrist watch" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $w6name . " " . $w6disctext?></h5>
            <p class="card-text">
            <?php echo $w6desc?>
            </p>
          </div>
          <p class="card-text ml-3"><span class="<?php echo $w6pbdt?>">US $<?php echo $w6price?></span>US $<?php echo $w6sale?></p>
          <p class="card-text ml-3">Shipping: $<?php echo $w6shipping?></p>
          <p class="card-text ml-3">Rating: <?php echo $w6rating?></p>
          <p class="card-text ml-3 <?php echo $w6stocktextColor?>"><?php echo $w6stocktext?></p>
          <p class="card-text ml-3">Total: $<?php echo $w6totalPrice?></p>
          <div class="card-body">
            <a href="#" class="btn <?php echo $w6btncolor?> btn-block"><?php echo $w6btntxt?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
    integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
    crossorigin="anonymous"></script>
</body>

</html>